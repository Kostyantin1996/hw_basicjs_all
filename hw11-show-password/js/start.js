let form = document.querySelector('.password-form');

form.addEventListener('click', (event) => {
    if (event.target.tagName === "I"){
        let eye = event.target;
        
        eye.classList.toggle("fa-eye");
        eye.classList.toggle("fa-eye-slash");

        let wrapper = eye.closest(".input-wrapper");
        let input = wrapper.querySelector("input");
        
        if(input.type === "password"){
            input.type = "text";
        } else{
            input.type = "password";
        }
    }
});

let submitBtn = document.getElementsByClassName('btn');

submitBtn[0].addEventListener("click", (event) => {
    event.preventDefault();

    let passwordsNodes = Array.from(document.querySelectorAll(".password-form input[type=password]"));
    let passwords = passwordsNodes.map(item => item.value);
    let errorMsg = document.getElementById("error");

    if(!passwords[0] || !passwords[1] || passwords[0] !== passwords[1]){
        errorMsg.classList.remove("hidden");
    } else {
        errorMsg.classList.add("hidden");
        alert("You are welcome");
    }
});


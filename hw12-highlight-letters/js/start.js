const handler = event => {
    let key = event.key.toLowerCase(); 
    let oldButton = document.querySelector('[data-active]');
    if (oldButton) { 
        oldButton.removeAttribute('data-active');
    }
    
    let button = document.querySelector(`[data-key=${key}]`);
    
    if (button) {
        button.setAttribute('data-active', '');
    }
    }
    document.addEventListener('keyup', handler);
    
let paragraphs = document.getElementsByTagName('p');
for (let item of paragraphs) {
    item.style.backgroundColor = "#ff0000";
}

let idElem = document.getElementById('optionsList');
let idElemParent = idElem.parentElement;
let idChildNodes = idElem.childNodes;
console.log(idElem);
console.log(idElemParent);

for (let item of idChildNodes) {
    console.log(item.nodeName + " " + item.nodeType);
}

let textParagraph = document.getElementById('testParagraph');
textParagraph.innerText = 'This is a paragraph';

let header = document.getElementsByClassName('main-header');
for (const item of header) {
    let headerChildren = item.children;
    console.log(headerChildren);

    for (const elem of headerChildren) {
        elem.classList.add('nav-item');
        console.log(elem);
    }
}

let section = document.getElementsByClassName('section-title');
console.log(section);
for (const item of section) {
    item.classList.remove('section-title');
    console.log(item);
}


